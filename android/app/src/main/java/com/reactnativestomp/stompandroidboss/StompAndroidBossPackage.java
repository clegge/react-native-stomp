package com.reactnativestomp.stompandroidboss;

/**
 * Created by clegge on 7/12/17.
 */

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.reactnativestomp.stompandroidboss.StompAndroidBossModule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StompAndroidBossPackage implements ReactPackage {
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(new StompAndroidBossModule(reactContext));

        return modules;
    }

    @Override
    public List<Class<? extends JavaScriptModule>> createJSModules() {
        return Collections.emptyList();  //must enhance from generated boilerplate
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList(); //must enhance from generated boilerplate
    }
}
