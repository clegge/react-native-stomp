package com.reactnativestomp.stompandroidboss;

import ua.naiksoftware.stomp.LifecycleEvent;

/**
 * Created by clegge on 7/14/17.
 */

public class LifecycleEventHelper {
    public static String getEventString(LifecycleEvent lifecycleEvent){
        LifecycleEvent.Type lifeCycleEventType = lifecycleEvent.getType();
        String eventString = "";
        switch (lifeCycleEventType) {
            case OPENED:
                return ("CONNECTION_OPENED");
            case ERROR:
                return ("CONNECTION_ERROR");
            case CLOSED:
                return ("CONNECTION_CLOSED");
            default:
                return ("UNKNOWN EVENT");
        }
    }
}
