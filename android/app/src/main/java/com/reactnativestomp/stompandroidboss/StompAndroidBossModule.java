package com.reactnativestomp.stompandroidboss;


import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;


import java.util.HashMap;

import okhttp3.WebSocket;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import ua.naiksoftware.stomp.LifecycleEvent;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.client.StompClient;

/**
 * Created by clegge on 7/12/17.
 */

public class StompAndroidBossModule extends ReactContextBaseJavaModule {
    //https://source.android.com/source/code-style#follow-field-naming-conventions
    public static final String TAG = "StompAndroidBossModule";
    StompClient mStompClient;
    ReactApplicationContext mReactContext;

    Callback mCBForStompEvents = null;
    Callback mCBForStompErrors = null;
    Callback mCBForConsoleLog = null;

    HashMap<String, Subscription> mSubscriptionMap = new HashMap<String, Subscription>();

    String mStompServerAddress = null;
    String mStompServerHandle = null;
    int mStompServerPort = 8080;

    LifecycleEvent.Type mStompConnectionStatus = LifecycleEvent.Type.CLOSED;

    public StompAndroidBossModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    private void connectToStomp(){
        String uri = "ws://" + this.mStompServerAddress + ":" + String.valueOf(this.mStompServerPort) + "/" + this.mStompServerHandle + "/websocket";
        this.conLog("Attempting to connect to:" + uri);
        mStompClient = Stomp.over(WebSocket.class, "ws://10.114.40.16:8080/stomp/websocket");
        mStompClient.connect();
        mStompClient.lifecycle().subscribe(lifecycleEvent -> {
            this.onStompLifeCycleEvent(lifecycleEvent);
        });
    }

    private void onStompLifeCycleEvent(LifecycleEvent lifecycleEvent){
        LifecycleEvent.Type lifeCycleEventType = lifecycleEvent.getType();
        this.mStompConnectionStatus = lifeCycleEventType;
        this.sendLifeCycleEventTypeToClient(lifecycleEvent);
        switch (lifeCycleEventType) {
            case OPENED:
                break;
            case ERROR:
                this.sendErrorToClient("Life Cycle Error", lifecycleEvent.getException());
                break;
            case CLOSED:
                break;
        }
    }

    private void sendLifeCycleEventTypeToClient(LifecycleEvent lifeCycleEvent){
        if(this.mCBForStompEvents != null){
            this.mCBForStompEvents.invoke(LifecycleEventHelper.getEventString(lifeCycleEvent));
        }else{
            Log.w(TAG, "Lifecycle Callback not set");
        }
    }

    private void conLog(String message){
        Log.d(TAG, message);
        if(this.mCBForConsoleLog != null){
            try{
                this.mCBForConsoleLog.invoke(message);
            }catch (Exception e){
                Log.e(TAG, e.getMessage());
            }
        }else{
            Log.w(TAG, "Console Log Callback not set");
        }
    }

    private void sendErrorToClient(String message, Exception exception){
        Log.e(TAG, message, exception);
        if(this.mCBForStompErrors != null){
            this.mCBForStompErrors.invoke(message, exception.getMessage());
        }else{
            Log.w(TAG, "Error Callback not set");
        }
    }


    @ReactMethod
    public void subscribe(String id, String destination, Callback callbackTransport, Callback callbackError){
        Subscription subscription = new Subscription(id, destination, callbackTransport, callbackError);
        this.updateSubscriptionMap(subscription);
        if(this.mStompClient.isConnected()){
            this.mStompClient.topic(destination)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(topicMessage -> {
                        synchronized (this.mSubscriptionMap){
                            this.mSubscriptionMap.get(id).getCallbackTransport().invoke(topicMessage.getPayload());
                            Log.d(TAG, "Received " + topicMessage.getPayload());
                        }
                    });
        } else{
            this.conLog("Cannnot subscribe to " + destination + " because client is not connected!");
        }
    }

    private void updateSubscriptionMap(Subscription subscription){
        this.mSubscriptionMap.put(subscription.getId(), subscription);
    }

    @ReactMethod
    public void updateSubscriptionCallback(String id, String type, Callback callback){
        switch(type){
            case "TRANSPORT_CALLBACK":
                this.mSubscriptionMap.get(id).setCallbackTransport(callback);
                break;
            case "ERROR_CALLBACK":
                this.mSubscriptionMap.get(id).setCallbackError(callback);
                break;
            default:
                this.conLog("updateSubscriptionCallback failed, type: " + type + " does not exist!");
        }
    }

    @ReactMethod
    public void connect(String stompServerAddress, int stompServerPort, String stompServerHandle){
        this.mStompServerAddress = stompServerAddress;
        this.mStompServerPort = stompServerPort;
        this.mStompServerHandle = stompServerHandle;
        this.connectToStomp();
    }


    @ReactMethod
    public void registerCallbackEvent(Callback callback){
        this.mCBForStompEvents = callback;
    }

    @ReactMethod
    public void registerCallbackError(Callback callback){
        this.mCBForStompErrors = callback;
    }

    @ReactMethod
    public void registerCallbackConsoleLog(Callback callback){
        this.mCBForConsoleLog = callback;
    }

    @Override
    public String getName() {
        return "StompAndroidBoss";
    }
}
