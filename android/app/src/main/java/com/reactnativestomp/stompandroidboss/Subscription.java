package com.reactnativestomp.stompandroidboss;

import com.facebook.react.bridge.Callback;

/**
 * Created by clegge on 7/13/17.
 */

public class Subscription {
    private String id;
    private String destination;
    private Callback callbackTransport;
    private Callback callbackError;

    public Subscription(String id, String destination, Callback callbackTransport, Callback callbackError) {
        this.id = id;
        this.destination = destination;
        this.callbackTransport = callbackTransport;
        this.callbackError = callbackError;
    }

    public void setCallbackTransport(Callback callbackTransport) {
        this.callbackTransport = callbackTransport;
    }

    public void setCallbackError(Callback callbackError) {
        this.callbackError = callbackError;
    }

    public String getId() {
        return id;
    }

    public String getDestination() {
        return destination;
    }

    public Callback getCallbackTransport() {
        return callbackTransport;
    }

    public Callback getCallbackError() {
        return callbackError;
    }
}
