package com.reactnativestomp;

import android.os.Bundle;
import android.util.Log;

import com.facebook.react.ReactActivity;

import okhttp3.WebSocket;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.client.StompClient;

public class MainActivity extends ReactActivity {
    StompClient mStompClient;
    final String TAG = "MainActivity";
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "reactNativeStomp";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
