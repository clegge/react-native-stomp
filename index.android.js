/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

/**
 * INITIAL

 positive callback
 error callback
 json payload : stump addy
 : initial TOPIC

 ONLOGIN
 : array of topics

 onstore create
 : array of topics

 loggin callback


 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Button,
    View,
} from 'react-native';

import StompAndroidBoss from './StompAndroidBoss'
import CallbackTypes from './constants/CallbackTypes'
import StompConnectionEvents from './constants/StompConnectionEvents'

const STOMP_SERVER_ADDRESS = "10.114.40.16";
const STOMP_SERVER_HANDLE = "stomp";
const STOMP_SERVER_PORT = 8080;

export const SUBSCRIPTION_IDS = {
    SUBSCRIPTION_MESSAGE: 'SUBSCRIPTION_MESSAGE',
    SUBSCRIPTION_LOGIN: 'SUBSCRIPTION_LOGIN',
    SUBSCRIPTION_STORE_CREATE: 'SUBSCRIPTION_STORE_CREATE',
};
export default class reactNativeStomp extends Component {
    constructor(props) {
        super(props);
        this.receiveCreateStoreMessagesErr = this.receiveCreateStoreMessagesErr.bind(this);
        this.receiveCreateStoreMessages = this.receiveCreateStoreMessages.bind(this);
        this.receiveLoginMessages = this.receiveLoginMessages.bind(this);
        this.receiveLoginMessagesErr = this.receiveLoginMessagesErr.bind(this);
        this.receiveStompMessages = this.receiveStompMessages.bind(this);
        this.receiveStompMessagesErr = this.receiveStompMessagesErr.bind(this);
        this.receiveStompErrors = this.receiveStompErrors.bind(this);
        this.recieveStompLogs = this.recieveStompLogs.bind(this);
        this.receiveStompLifeCyleEvent = this.receiveStompLifeCyleEvent.bind(this);
        this.renderLogin = this.renderLogin.bind(this);
        this.renderStoreCreate = this.renderStoreCreate.bind(this);
        this.onLoginClick = this.onLoginClick.bind(this);
        this.onCreateStoreClick = this.onCreateStoreClick.bind(this);
        this.state = {
            _message: 'Your quotes are coming (if you modified the hard coded IP)',
            _messageSubscribed: false,
            _loginMessage: 'Waiting on login subscription',
            _storeCreateMessage: 'Waiting on store create subscription',
            _loggedIn: false,
            _storeCreated: false,
            _lastStompEvent: null
        };
    }

    componentWillMount() {
        console.log('componentDidMount')
        StompAndroidBoss.registerCallbackEvent(this.receiveStompLifeCyleEvent);
        StompAndroidBoss.registerCallbackError(this.receiveStompErrors);
        StompAndroidBoss.registerCallbackConsoleLog(this.recieveStompLogs);
        StompAndroidBoss.connect(STOMP_SERVER_ADDRESS, STOMP_SERVER_PORT, STOMP_SERVER_HANDLE)
    }

    receiveStompErrors(message, exception){
        console.log('** receiveStompErrors')
        StompAndroidBoss.registerCallbackError(this.receiveStompErrors)
        console.error(`[StompAndroidBoss] : ${message} | EXCEPTION: ${exception}`);

    }

    recieveStompLogs(message){
        StompAndroidBoss.registerCallbackConsoleLog(this.recieveStompLogs);
        console.log('** recieveStompLogs')
        console.log(`[StompAndroidBoss] : ${message}`);
    }

    receiveStompLifeCyleEvent(lifeCyleEvent){
        console.log('** receiveStompLifeCyleEvent')
        console.log(`[StompAndroidBoss] : ${lifeCyleEvent}`);
        StompAndroidBoss.registerCallbackEvent(this.receiveStompLifeCyleEvent)

        console.log(`Attempting to subscribe to ${SUBSCRIPTION_IDS.SUBSCRIPTION_MESSAGE}`);
        if(lifeCyleEvent === StompConnectionEvents.CONNECTION_OPENED && this.state._messageSubscribed === false){
            StompAndroidBoss.subscribe(SUBSCRIPTION_IDS.SUBSCRIPTION_MESSAGE, '/topic/quotes', this.receiveStompMessages, this.receiveStompMessagesErr)
        }

        this.setState({
            _lastStompEvent :lifeCyleEvent
        })
    }

    ///topic/quotes
    receiveStompMessages(payload) {
        if(payload){
            StompAndroidBoss.updateSubscriptionCallback(SUBSCRIPTION_IDS.SUBSCRIPTION_MESSAGE, CallbackTypes.TRANSPORT_CALLBACK, this.receiveStompMessages);
            let messageOBj = JSON.parse(payload);
            //console.log(`[StompAndroidBoss] : ${messageOBj}`);
            this.setState({
                _message: messageOBj.content.quoteText
            });
        } else{
            console.log('receiveStompMessages fired, no payload!')
        }
    }

    receiveStompMessagesErr(message, exception){
        StompAndroidBoss.updateSubscriptionCallback(SUBSCRIPTION_IDS.SUBSCRIPTION_MESSAGE, CallbackTypes.ERROR_CALLBACK, this.receiveStompMessagesErr);
        console.error(`[StompAndroidBoss] : ${message} | EXCEPTION: ${exception}`);
    }

    onLoginClick(){
        console.log('onLoginClick');
        this.setState({
            _loggedIn: true
        });
        StompAndroidBoss.subscribe(SUBSCRIPTION_IDS.SUBSCRIPTION_LOGIN, '/topic/footballScores', this.receiveLoginMessages, this.receiveLoginMessagesErr)
    }

    receiveLoginMessages(payload){
        if(payload){
            StompAndroidBoss.updateSubscriptionCallback(SUBSCRIPTION_IDS.SUBSCRIPTION_LOGIN, CallbackTypes.TRANSPORT_CALLBACK, this.receiveLoginMessages);
            let messageOBj = JSON.parse(payload);
            this.setState({
                _loginMessage: messageOBj.content.score
            });
        } else{
            console.log('receiveLoginMessages fired, no payload!')
        }
    }

    receiveLoginMessagesErr(message, exception){
        StompAndroidBoss.updateSubscriptionCallback(SUBSCRIPTION_IDS.SUBSCRIPTION_LOGIN, CallbackTypes.ERROR_CALLBACK, this.receiveStompMessagesErr);
        console.error(`[StompAndroidBoss] : ${message} | EXCEPTION: ${exception}`);
    }

    onCreateStoreClick(){
        console.log('onCreateStoreClick');
        this.setState({
            _storeCreated: true
        });
        StompAndroidBoss.subscribe(SUBSCRIPTION_IDS.SUBSCRIPTION_STORE_CREATE, '/topic/fastFoodMeals', this.receiveCreateStoreMessages, this.receiveCreateStoreMessagesErr)
    }

    receiveCreateStoreMessages(payload){
        if(payload){
            StompAndroidBoss.updateSubscriptionCallback(SUBSCRIPTION_IDS.SUBSCRIPTION_STORE_CREATE, CallbackTypes.TRANSPORT_CALLBACK, this.receiveCreateStoreMessages);
            let messageOBj = JSON.parse(payload);
            this.setState({
                _storeCreateMessage: messageOBj.content.food
            });
        } else{
            console.log('receiveCreateStoreMessages fired, no payload!')
        }
    }

    receiveCreateStoreMessagesErr(message, exception){
        StompAndroidBoss.updateSubscriptionCallback(SUBSCRIPTION_IDS.SUBSCRIPTION_STORE_CREATE, CallbackTypes.ERROR_CALLBACK, this.receiveCreateStoreMessagesErr);
        console.error(`[StompAndroidBoss] : ${message} | EXCEPTION: ${exception}`);
    }

    renderLogin(){
        if(this.state._loggedIn){
            return(
                <Text style={styles.message}>
                    {this.state._loginMessage}
                </Text>
            )
        }
        return(
            <Button title="Simulate Login" onPress={this.onLoginClick}/>
        )
    }

    renderStoreCreate(){
        if(this.state._storeCreated){
            return(
                <Text style={styles.message}>
                    {this.state._storeCreateMessage}
                </Text>
            )
        }
        return(
            <Button title="Simulate Create Store" onPress={this.onCreateStoreClick}/>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <View style={styles.labelWrapper}>
                        <Text style={styles.labelText}>
                            On Connect:
                        </Text>
                    </View>
                    <View style={styles.messageWrapper}>
                        <Text style={styles.message}>
                            {this.state._message}
                        </Text>
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.labelWrapper}>
                        <Text style={styles.labelText}>
                            On Login:
                        </Text>
                    </View>
                    <View style={styles.messageWrapper}>
                        {this.renderLogin()}
                    </View>
                </View>
                <View style={styles.row}>
                    <View style={styles.labelWrapper}>
                        <Text style={styles.labelText}>
                            On Store Create:
                        </Text>
                    </View>
                    <View style={styles.messageWrapper}>
                        {this.renderStoreCreate()}
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    labelWrapper:{
        width: 150,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingRight:5,
    },

    messageWrapper:{
      flex: 1,
        paddingLeft:5,
    },

    row: {
        flexDirection: 'row',
        margin: 5,
    },


    labelText: {
        fontWeight: 'bold',

    },

    message: {

    },
});

AppRegistry.registerComponent('reactNativeStomp', () => reactNativeStomp);
